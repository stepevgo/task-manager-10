package ru.t1.stepanishchev.tm.api;

import ru.t1.stepanishchev.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
