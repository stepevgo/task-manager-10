package ru.t1.stepanishchev.tm.api;

import ru.t1.stepanishchev.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    void clear();
}
