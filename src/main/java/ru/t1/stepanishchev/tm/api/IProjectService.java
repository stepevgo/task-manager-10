package ru.t1.stepanishchev.tm.api;

import ru.t1.stepanishchev.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
