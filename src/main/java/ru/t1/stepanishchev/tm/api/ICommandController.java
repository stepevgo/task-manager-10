package ru.t1.stepanishchev.tm.api;

public interface ICommandController {

    void showInfo();

    void showArgumentError();

    void showCommandError();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();
}
