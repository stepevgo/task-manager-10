package ru.t1.stepanishchev.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
