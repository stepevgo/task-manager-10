package ru.t1.stepanishchev.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
